# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name   = "gupaulhe"
  user_domain_name = "ucafr"
  tenant_name = "iut_prj_gupaulhe"
  password    = ""
  auth_url    = "https://openstack.iut.uca.fr:5000"
  region      = "RegionOne"
}

