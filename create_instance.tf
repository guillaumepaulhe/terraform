resource "openstack_compute_instance_v2" "debian" {
  name            = "debian"
  image_id        = "fdfa51a1-03a8-4a18-9fbb-e71f771b2822"
  flavor_name       = "m1_small"
  key_pair        = "TP2"
  security_groups = ["default", "secgroup_1"]

  network {
    name = "network_1"
  }
  depends_on = [openstack_compute_secgroup_v2.secgroup_1, openstack_networking_network_v2.network_1 ]
}

